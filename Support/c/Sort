/* Copyright 1998 Acorn Computers Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <stdlib.h>
#include <string.h>

#include "Desk.DeskMem.h"
#include "Desk.LinkList.h"
#include "Desk.Debug.h"

#include "AcornNC.DirScan.h"



static AcornNC_dirscan_sort*	global_sorts;


static int	Compare2( AcornNC_dirscan_fileblock* a, AcornNC_dirscan_fileblock* b, AcornNC_dirscan_sort sort)
	{
	if ( sort == AcornNC_dirscan_sort_NAME)	return strcmp( a->info.name, b->info.name);
	if ( sort == AcornNC_dirscan_sort_TYPE)	return AcornNC_DirScan_GetType( a) - AcornNC_DirScan_GetType( b);
	if ( sort == AcornNC_dirscan_sort_SIZE)	return a->info.length - b->info.length;
	if ( sort == AcornNC_dirscan_sort_SIZE)	{
		int*	date_a = (int*) a->info.date;
		int*	date_b = (int*) b->info.date;
		if ( date_a[1] == date_b[1])	return date_a[0] - date_b[0];
		return date_a[1] - date_b[1];
		}
	if ( sort == AcornNC_dirscan_sort_OBJTYPE)	return a->info.objtype - b->info.objtype;

	return 0;
	}


static int	CompareFn( const void* a, const void* b)
	{
	int	c=0;
	int	sortindex;

	AcornNC_dirscan_fileblock**	aa = (AcornNC_dirscan_fileblock**) a;
	AcornNC_dirscan_fileblock**	bb = (AcornNC_dirscan_fileblock**) b;

	AcornNC_dirscan_fileblock*	aaa = *aa;
	AcornNC_dirscan_fileblock*	bbb = *bb;

	for ( sortindex=0; global_sorts[ sortindex]!=AcornNC_dirscan_sort_NONE; sortindex++)	{
		c = Compare2( aaa, bbb, global_sorts[ sortindex]);
		if ( c != 0)	break;
		}

	return c;
	}



void	AcornNC_DirScan_Sort( AcornNC_dirscan_allfilesblock* allfiles, AcornNC_dirscan_sort* sorts)
	{
	int				i;
	AcornNC_dirscan_fileblock*	file;
	AcornNC_dirscan_fileblock**	filearray;
	int				n = Desk_LinkList_ListLength( &allfiles->files);

	filearray = Desk_DeskMem_CallocType( n, AcornNC_dirscan_fileblock*);

	global_sorts	= sorts;

	for	(
		file = Desk_LinkList_FirstItem( &allfiles->files), i=0;
		file;
		file = Desk_LinkList_NextItem( &file->header)
		)
		{
		Desk_Debug_Assert( i<n);
		filearray[i] = file;
		i++;
		}

	qsort( filearray, n, sizeof( AcornNC_dirscan_fileblock*), CompareFn);

	for ( i=0; i<n; i++)	{
		Desk_LinkList_Unlink( &allfiles->files, &filearray[ i]->header);
		Desk_LinkList_AddToTail( &allfiles->files, &filearray[ i]->header);
		}

	Desk_DeskMem_Free( filearray);
	}
